from django.shortcuts import render, redirect, get_object_or_404
from django.views import View
from django.views.generic import ListView
from accounts.models import User


class SearchPairListView(ListView):
    model = User
    template_name = 'matching/search_pair.html'
    context_object_name = 'random_user'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        current_user = self.request.user
        opposite_gender = 'М' if current_user.gender == 'Ж' else 'Ж'
        context['random_user'] = User.objects.filter(
            gender=opposite_gender).exclude(id=current_user.id).order_by('?').first()
        return context


class LikeUserView(View):
    def post(self, request, user_id):
        user_to_like = get_object_or_404(User, id=user_id)
        current_user = request.user
        current_user.likes.add(user_to_like)
        return redirect('matching:search_pair')


class DislikeUserView(View):
    def post(self, request, user_id):
        user_to_dislike = get_object_or_404(User, id=user_id)
        current_user = request.user
        current_user.dislikes.add(user_to_dislike)
        return redirect('matching:search_pair')
