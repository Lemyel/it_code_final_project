from django.urls import path
from . import views

app_name = 'matching'

urlpatterns = [
    path('search_pair/', views.SearchPairListView.as_view(), name='search_pair'),
    path('like/<int:user_id>/', views.LikeUserView.as_view(), name='like'),
    path('dislike/<int:user_id>/', views.DislikeUserView.as_view(), name='dislike'),
]

