from django.shortcuts import render, redirect
from django.views.generic import ListView, CreateView
from .models import Story
from .forms import StoryForm
from django.urls import reverse_lazy


class StoryListView(ListView):
    model = Story
    template_name = 'stories/story_list.html'
    context_object_name = 'stories'
    ordering = ['-created_at']

    def get_queryset(self):
        queryset = super().get_queryset()
        search_query = self.request.GET.get('search')
        if search_query:
            queryset = queryset.filter(topic__iregex=search_query)
        return queryset


class AddStoryView(CreateView):
    model = Story
    form_class = StoryForm
    template_name = 'stories/add_story.html'
    success_url = reverse_lazy('stories:story_list')

    def form_valid(self, form):
        form.instance.author = self.request.user
        return super().form_valid(form)
