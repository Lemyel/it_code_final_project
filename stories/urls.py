from django.urls import path
from . import views

app_name = 'stories'

urlpatterns = [
    path('', views.StoryListView.as_view(), name='story_list'),
    path('add/', views.AddStoryView.as_view(), name='add'),
]
