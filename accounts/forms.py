from django import forms
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from .models import User
from django.contrib import messages

class RegistrationForm(UserCreationForm):
    username = forms.CharField(
        label=False,
        widget=forms.TextInput(attrs={'placeholder': 'Введите логин'})
    )
    password1 = forms.CharField(
        label=False,
        widget=forms.PasswordInput(attrs={'placeholder': 'Введите пароль'})
    )
    password2 = forms.CharField(
        label=False,
        widget=forms.PasswordInput(attrs={'placeholder': 'Подтвердите пароль'})
    )
    name = forms.CharField(
        label=False,
        widget=forms.TextInput(attrs={'placeholder': 'Введите ваше имя'})
    )
    age = forms.IntegerField(
        label=False,
        min_value=0,
        initial=18,
        widget=forms.NumberInput(attrs={'placeholder': 'Введите ваш возраст'})
    )
    city = forms.CharField(
        label=False,
        widget=forms.TextInput(attrs={'placeholder': 'Введите ваш город'})
    )
    gender = forms.ChoiceField(
        label=False,
        choices=User.CHOICE_GENDER,
    )
    bio = forms.CharField(
        label=False,
        widget=forms.Textarea(attrs={'placeholder': 'Напишите немного о себе'})
    )
    photo = forms.ImageField(
        label=False,
        required=False,
    )

    
    class Meta:
        model = User
        fields = ['username',
                'password1',
                'password2',
                'name',
                'age',
                'city',
                'gender',
                'bio',
                'photo']


class LoginForm(AuthenticationForm):
    username = forms.CharField(
        widget=forms.TextInput(attrs={'placeholder': 'Логин'}))
    password = forms.CharField(
        widget=forms.PasswordInput(attrs={'placeholder': 'Пароль'}))


class ProfileUpdateForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ['name', 'age', 'city', 'gender', 'bio', 'photo']
