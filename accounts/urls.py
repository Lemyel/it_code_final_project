from django.urls import path
from . import views

app_name = 'accounts'

urlpatterns = [
    path('register/', views.RegisterFormView.as_view(), name='register'),
    path('login/', views.LoginFormView.as_view(), name='login'),
    path('logout/', views.logout_view, name='logout'),
    path('delete/', views.DeleteAccountView.as_view(), name='delete'),
    path('cancel-delete/', views.cancel_delete, name='cancel_delete'),
    path('profile/update/', views.ProfileUpdateView.as_view(), name='update'),
]
