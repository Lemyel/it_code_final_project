from django.db import models
from django.contrib.auth.models import AbstractUser, Group, Permission


class User(AbstractUser):
    CHOICE_GENDER = (
        ('М', 'мужчина'),
        ('Ж', 'женщина'),
    )

    name = models.CharField(max_length=255)
    age = models.PositiveIntegerField(default=18)
    city = models.CharField(max_length=255)
    gender = models.CharField(max_length=255, choices=CHOICE_GENDER, default=CHOICE_GENDER[1][0])
    bio = models.TextField()
    photo = models.ImageField(upload_to='users/', default='users/default_avatar.jpg')

    likes = models.ManyToManyField('self', blank=True, symmetrical=False, related_name='liked_users')
    dislikes = models.ManyToManyField('self', blank=True, symmetrical=False, related_name='disliked_users')

    user_permissions = models.ManyToManyField(
        Permission,
        verbose_name='user permissions',
        blank=True,
    )

    groups = models.ManyToManyField(
        'auth.Group',
        verbose_name='groups',
        blank=True,
        help_text='Группы, к которым принадлежит этот пользователь.',
    )

    def __str__(self):
        return f'Имя - {self.name}, Логин - {self.username}, пол - {self.gender}, возраст - {self.age}, город - {self.city}'
