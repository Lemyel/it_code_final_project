from django.urls import reverse_lazy
from django.views.generic import CreateView, TemplateView, DeleteView, UpdateView
from .forms import RegistrationForm, LoginForm, ProfileUpdateForm
from django.contrib.auth.views import LoginView
from django.contrib.auth import login, logout
from django.shortcuts import redirect
from django.contrib.auth.mixins import LoginRequiredMixin
from .models import User
from django.contrib.auth import update_session_auth_hash


class RegisterFormView(CreateView):
    form_class = RegistrationForm
    template_name = 'accounts/register.html'
    success_url = reverse_lazy('accounts:login')


class LoginFormView(LoginView):
    form_class = LoginForm
    template_name = 'accounts/login.html'

    def form_valid(self, form):
        user = form.get_user()
        login(self.request, user)
        return redirect('core:index')


def logout_view(request):
    logout(request)
    return redirect('core:index')


class DeleteAccountView(LoginRequiredMixin, DeleteView):
    model = User
    template_name = 'accounts/delete.html'
    success_url = reverse_lazy('core:index')

    def get_object(self, queryset=None):
        return self.request.user


def cancel_delete(request):
    return redirect('core:profile')


class ProfileUpdateView(LoginRequiredMixin, UpdateView):
    model = User
    form_class = ProfileUpdateForm
    template_name = 'accounts/update.html'
    success_url = reverse_lazy('core:profile')

    def get_object(self, queryset=None):
        return self.request.user