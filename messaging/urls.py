from django.urls import path
from .views import DialogListView, DialogDetailView, MessageCreateView

app_name = 'messaging'

urlpatterns = [
    path('', DialogListView.as_view(), name='dialog_list'),
    path('dialog/<int:dialog_id>/', DialogDetailView.as_view(), name='dialog_detail'),
    path('dialog/<int:dialog_id>/message/create/', MessageCreateView.as_view(), name='message_create'),
]
