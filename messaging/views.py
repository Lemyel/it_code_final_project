from django.shortcuts import render, get_object_or_404, redirect
from django.views import View
from .models import Dialog, Message

class DialogListView(View):
    def get(self, request):
        user = request.user
        dialogs = Dialog.objects.filter(user1=user) | Dialog.objects.filter(user2=user)

        matches = user.likes.filter(liked_users=user)

        existing_dialogs = dialogs.values_list('user1_id', 'user2_id')

        # Формируем список новых диалогов, которых еще нет у пользователя
        new_dialogs = []
        for match in matches:
            if (user.id, match.id) not in existing_dialogs and (match.id, user.id) not in existing_dialogs:
                dialog = Dialog.create_dialog(user, match)
                if dialog is not None:
                    new_dialogs.append(dialog)

        dialogs |= Dialog.objects.filter(id__in=[dialog.id for dialog in new_dialogs])

        return render(request, 'messaging/dialog_list.html', {'dialogs': dialogs})


class DialogDetailView(View):
    def get(self, request, dialog_id):
        dialog = get_object_or_404(Dialog, id=dialog_id)
        messages = dialog.message_set.order_by('created_at') 
        return render(request, 'messaging/dialog_detail.html', {'dialog': dialog, 'messages': messages})

class MessageCreateView(View):
    def post(self, request, dialog_id):
        dialog = get_object_or_404(Dialog, id=dialog_id)
        content = request.POST.get('content')
        sender = request.user
        message = Message(dialog=dialog, sender=sender, content=content)
        message.save()
        return redirect('messaging:dialog_detail', dialog_id=dialog_id)
