from django.db import models
from accounts.models import User

class Dialog(models.Model):
    user1 = models.ForeignKey(User, on_delete=models.CASCADE, related_name='dialogs1')
    user2 = models.ForeignKey(User, on_delete=models.CASCADE, related_name='dialogs2')

    def __str__(self):
        return f"{self.user1.username} - {self.user2.username}"

    @classmethod
    def create_dialog(cls, user1, user2):
        if cls.objects.filter(user1=user2, user2=user1).exists():
            return cls.objects.filter(user1=user2, user2=user1).first()
        
        if user1.likes.filter(id=user2.id).exists() and user2.likes.filter(id=user1.id).exists():
            dialog = cls(user1=user1, user2=user2)
            dialog.save()
            return dialog
        
        return None
    
class Message(models.Model):
    dialog = models.ForeignKey(Dialog, on_delete=models.CASCADE)
    sender = models.ForeignKey(User, on_delete=models.CASCADE)
    content = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)
