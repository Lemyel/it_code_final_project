from django.shortcuts import render
from django.views.generic import TemplateView, DetailView
from accounts.models import User
from django.contrib.auth.mixins import LoginRequiredMixin
# Create your views here.


class Index(TemplateView):
    template_name = 'core/index.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['user'] = self.request.user
        return context


class ProfileView(TemplateView):
    template_name = 'core/profile.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['user'] = self.request.user
        return context
